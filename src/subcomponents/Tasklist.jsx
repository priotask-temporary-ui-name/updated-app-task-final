import Table from "react-bootstrap/Table";
import TaskRow from "./TaskRow";

const Tasklist = ({ tasks, deleteTask, isDark, toggleTask }) => {
   return (
      <div className="custom-table-container  mt-4">
         <Table
            striped
            bordered
            hover
            responsive
            className={`${isDark ? "table-dark" : "table-light"}`}
         >
            <thead>
               <tr className={`borderTr ${isDark ? "dark" : ""} row-th`}>
                  <th className="th-weight" scope="col">
                     #
                  </th>
                  <th className="th-weight" scope="col">
                     Task Name
                  </th>
                  <th className="th-weight" scope="col">
                     Status
                  </th>
                  <th className="th-weight" scope="col">
                     Start Date
                  </th>
                  <th className="th-weight" scope="col">
                     Due Date
                  </th>
                  <th className="th-weight" scope="col">
                     Action
                  </th>
               </tr>
            </thead>
            <tbody>
               {tasks
                  .sort((a, b) => b.id - a.id)
                  .map((task, countNumber) => (
                     <tr key={task.id}>
                        <th className="row-th" scope="row">
                           {countNumber + 1}
                        </th>
                        <TaskRow
                           task={task}
                           deleteTask={deleteTask}
                           toggleTask={toggleTask}
                           // setStatus={setStatus}
                           // status={status}
                        />
                     </tr>
                  ))}
            </tbody>
         </Table>
      </div>
   );
};

export default Tasklist;
