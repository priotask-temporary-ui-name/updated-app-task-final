import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
// import useLocalStorage from "../hooks/useLocalStorage";

const TaskRow = ({ task, deleteTask, toggleTask }) => {
   const [dateTime, setDateTime] = useState("");
   const [isActivate, setIsActivate] = useState(task.isActive);

   useEffect(() => {
      const currentDateTime = new Date();
      const formattedDateTime = currentDateTime.toLocaleString();
      setDateTime(formattedDateTime);
   }, []);

   const handleStackClick = (e) => {
      setIsActivate(!isActivate);
      toggleTask(task.id);
   };

   return (
      <>
         {/* <th scope="row">{task.id}</th> */}
         <td className="task-name-td">{task.name}</td>
         {/* <td className="task-name-td">{task.isActive}</td> */}
         <td className="status-td">
            {task.isActive ? "🟢 Active" : "⚪ Inactive"}
         </td>
         <td className="start-date-td">{dateTime}</td>
         <td className="deadline-td">{task.deadline}</td>
         <td className="action">
            {task.isActive ? (
               <>
                  <button className="btn stacked mx-1 my-1 my-md-0" disabled>
                     Stacked
                  </button>
                  <Link to={`/stackboard`}>
                     <span className="badge bg-warning text-dark my-1 my-md-0">
                        See
                     </span>
                  </Link>
               </>
            ) : (
               <>
                  <button
                     onClick={handleStackClick}
                     className="btn btn-success stack mx-1 my-1 my-md-0"
                  >
                     Stack
                  </button>
                  <button
                     onClick={() => deleteTask(task.id)}
                     className="btn btn-danger delete mx-1 my-1 my-md-0"
                  >
                     Delete
                  </button>
               </>
            )}
         </td>
      </>
   );
};

export default TaskRow;
