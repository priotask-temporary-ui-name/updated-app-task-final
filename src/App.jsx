import { useState } from "react";
import { BrowserRouter, Routes, Route, Router } from "react-router-dom";
import VerticalNavbar from "./header/VerticalNavbar";
import HorizontalNavbar from "./header/HorizontalNavbar";
import Calendar from "./components/Calendar";
import Dashboard from "./components/Dashboard";
import Notifications from "./components/Notifications";
import StackBoard from "./components/StackBoard";
import StartHere from "./components/StartHere";

function App() {
   const [sidebarHidden, setSidebarHidden] = useState(false);
   const [isDark, setIsDark] = useState(false);

   const toggleSidebar = () => {
      setSidebarHidden(!sidebarHidden);
   };

   const makeDark = () => {
      setIsDark(true);
   };
   const makeLight = () => {
      setIsDark(false);
   };

   return (
      <>
         <div className={` ${isDark ? "superText" : ""}`}>
            <BrowserRouter>
               <div className="container-fluid g-0 d-flex main-container">
                  <VerticalNavbar
                     makeLight={makeLight}
                     makeDark={makeDark}
                     isDark={isDark}
                     toggleSidebar={toggleSidebar}
                     sidebarHidden={sidebarHidden}
                     setSidebarHidden={setSidebarHidden}
                  />
                  <div className="container-fluid g-0 d-block">
                     <div className="">
                        <div>
                           <HorizontalNavbar
                              isDark={isDark}
                              makeDark={makeDark}
                              toggleSidebar={toggleSidebar}
                              sidebarHidden={sidebarHidden}
                              setSidebarHidden={setSidebarHidden}
                              makeLight={makeLight}
                           />
                        </div>
                        <div className="routes">
                           <Routes>
                              <Route
                                 path="/calendar"
                                 element={
                                    <Calendar
                                       isDark={isDark}
                                       makeLight={makeLight}
                                    />
                                 }
                              ></Route>
                              <Route
                                 path="/dashboard"
                                 element={
                                    <Dashboard
                                       isDark={isDark}
                                       makeLight={makeLight}
                                    />
                                 }
                              ></Route>
                              <Route
                                 path="/notifications"
                                 element={
                                    <Notifications
                                       isDark={isDark}
                                       makeLight={makeLight}
                                    />
                                 }
                              ></Route>
                              <Route
                                 path="/stackboard"
                                 element={
                                    <StackBoard
                                       isDark={isDark}
                                       makeLight={makeLight}
                                    />
                                 }
                              ></Route>
                              <Route
                                 path="/"
                                 element={
                                    <StartHere
                                       isDark={isDark}
                                       makeLight={makeLight}
                                    />
                                 }
                              ></Route>
                           </Routes>
                        </div>
                     </div>
                  </div>
               </div>
            </BrowserRouter>
            <div
               className={`view-page-overlay-blur ${isDark ? "dark" : ""}`}
            ></div>

            <div className="view-page">
               <video autoPlay loop muted className="video">
                  <source
                     src="https://res.cloudinary.com/dv3x7qoak/video/upload/v1692071694/aaaa_ad9tya.mp4"
                     type="video/mp4"
                  />
               </video>
            </div>
         </div>
      </>
   );
}

export default App;
