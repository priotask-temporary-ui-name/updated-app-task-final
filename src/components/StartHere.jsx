import React, { useState } from "react";
import NaiveForm from "./NaiveForm";
import Tasklist from "../subcomponents/Tasklist";
import useLocalStorage from "../hooks/useLocalStorage";
import Swal from "sweetalert2";

const StartHere = ({ isDark, makeLight }) => {
   const [tasks, setTasks] = useLocalStorage("naive-bayes.tasks", []);

   const addTask = (task) => {
      setTasks((prevState) => [...prevState, task]);
      // return setTasks;
   };

   function deleteTask(id) {
      setTasks((prevState) => prevState.filter((t) => t.id !== id));
   }
   const toggleTask = (id) => {
      setTasks((prevState) =>
         prevState.map((t) =>
            t.id === id ? { ...t, isActive: !t.isActive } : t
         )
      );
   };

   const clearLocalStorage = () => {
      // localStorage.removeItem("naive-bayes.tasks");
      const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
            confirmButton: "btn btn-warning mx-1",
            cancelButton: "btn btn-danger mx-1",
            popup: "my-custom-popup-class",
         },
         buttonsStyling: false,
      });

      swalWithBootstrapButtons
         .fire({
            title: "Are you sure?",
            text: "You won't be able to revert this! This will delete all saved tasks.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Just cancel!",
            reverseButtons: true,
         })
         .then((result) => {
            if (result.isConfirmed) {
               localStorage.removeItem("naive-bayes.tasks");
               setTasks([]);
               swalWithBootstrapButtons.fire(
                  "Cleared!",
                  "All tasks has been deleted.",
                  "success"
               );
            } else if (
               /* Read more about handling dismissals below */
               result.dismiss === Swal.DismissReason.cancel
            ) {
               swalWithBootstrapButtons.fire(
                  "Cancelled",
                  "Task is still Task remains stored.",
                  "info"
               );
            }
         });
   };

   return (
      <>
         <span className="pages-title-predict">Predict Priority Level</span>
         <button
            type="button"
            className="btn btn-add-task"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
            data-bs-whatever="@mdo"
         >
            <i className="bx bx-plus"></i> Task
         </button>
         <NaiveForm
            addTask={addTask}
            //  status={status} setStatus={setStatus}
         />
         {tasks && (
            <Tasklist
               tasks={tasks}
               deleteTask={deleteTask}
               isDark={isDark}
               makeLight={makeLight}
               toggleTask={toggleTask}
            />
         )}
         <button
            className={`d-flex clear-local  menu-items ${isDark ? "dark" : ""}`}
            onClick={clearLocalStorage}
         >
            {" "}
            <span className="align-self-center">
               <i
                  className={`bx menu-icons ${
                     isDark ? "dark" : ""
                  } bx-trash align-self-center`}
               ></i>
            </span>
            <span className="align-self-center">Clear Local Storage</span>
         </button>
      </>
   );
};

export default StartHere;
