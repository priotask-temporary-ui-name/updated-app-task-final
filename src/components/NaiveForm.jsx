import React, { useState } from "react";

const NaiveForm = ({ addTask, isDark }) => {
   const [task, setTask] = useState("");
   const [difficulty, setDifficulty] = useState("0");
   const [deadline, setDeadline] = useState("");
   const [duration, setDuration] = useState("1");

   const handleFormSubmit = (e) => {
      e.preventDefault();
      addTask({
         id: Date.now(),
         name: task,
         isActive: false,
         difficulty: parseInt(difficulty),
         deadline: deadline,
         duration: parseInt(duration),
      });
      setTask("");
      setDifficulty("0");
      setDeadline("");
      setDuration("1");
   };

   const renderFormGroup = (labelText, id, children) => (
      <div className="my-4">
         <label htmlFor={id} className="form-label text-light">
            {labelText}
         </label>
         {children}
      </div>
   );

   return (
      <div>
         <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
         >
            <div className="modal-dialog">
               <div
                  className={`modal-content form-add-task ${
                     isDark ? "dark" : ""
                  }`}
               >
                  <div className="d-flex mt-2 mx-2">
                     <h1 className="modal-title" id="exampleModalLabel">
                        🔴🟢🟡
                     </h1>
                     <button
                        type="button"
                        className="btn-close ms-auto"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                     ></button>
                  </div>
                  <div className="modal-body">
                     <form onSubmit={handleFormSubmit}>
                        {renderFormGroup(
                           "Task Name",
                           "task",
                           <input
                              type="text"
                              id="task"
                              className="form-control"
                              value={task}
                              onInput={(e) => setTask(e.target.value)}
                              required
                              autoFocus
                              maxLength={60}
                              placeholder="Enter new task"
                           />
                        )}

                        {renderFormGroup(
                           "Difficulty",
                           "difficulty",
                           <select
                              id="difficulty"
                              className="form-select"
                              aria-label="Select difficulty level"
                              value={difficulty}
                              onChange={(e) => setDifficulty(e.target.value)}
                           >
                              <option value="0">Easy</option>
                              <option value="1">Moderate</option>
                              <option value="2">Hard</option>
                           </select>
                        )}

                        {renderFormGroup(
                           "Deadline",
                           "deadline",
                           <input
                              id="deadline"
                              className="date form-control"
                              type="date"
                              value={deadline}
                              onChange={(e) => setDeadline(e.target.value)}
                           />
                        )}

                        {renderFormGroup(
                           "Time Needed for Completion",
                           "needed_time",
                           <div className="scrollable-select">
                              <select
                                 id="needed_time"
                                 className="form-select"
                                 aria-label="Select time needed for completion"
                                 value={duration}
                                 onChange={(e) => setDuration(e.target.value)}
                              >
                                 <option value="1">1 hour</option>
                                 <option value="2">2 hours</option>
                                 <option value="3">3 hours</option>
                                 <option value="4">4 hours</option>
                                 <option value="5">5 hours</option>
                                 <option value="6">6 hours</option>
                                 <option value="7">7 hours</option>
                                 <option value="8">8 hours</option>
                                 <option value="9">9 hours</option>
                                 <option value="10">10 hours</option>
                              </select>
                           </div>
                        )}
                     </form>
                  </div>
                  <div className="m-3">
                     <button
                        type="button"
                        className="btn btn-secondary mx-2"
                        data-bs-dismiss="modal"
                     >
                        Close
                     </button>
                     <button
                        className="btn btn-success btn-predict mx-2"
                        aria-label="Predict Priority"
                        type="submit"
                        onClick={handleFormSubmit}
                     >
                        Predict
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
};

export default NaiveForm;
