import React from "react";

const Calendar = ({ isDark, makeLight }) => {
   return (
      <div>
         <span className="pages-title">Calendar</span>
         <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid
            placeat ad quibusdam corporis nostrum cupiditate qui velit
            consequuntur nobis sint quisquam, vero odit itaque non doloribus!
            Consequuntur quaerat officia libero nulla dolores eveniet incidunt
            aperiam non sapiente molestias quasi iure harum amet tenetur at
            praesentium magnam eius, necessitatibus obcaecati ipsum aspernatur
            facere ullam odit error. Itaque, assumenda illum, minima non quia
            voluptatibus autem libero perspiciatis veritatis tenetur soluta
            labore. Dolore porro rerum officia recusandae. Sint nam qui
            officiis, corporis unde ipsam veniam quae molestiae id nemo quam
            velit quasi similique rem obcaecati voluptas non eius dicta facere
            veritatis mollitia reprehenderit rerum praesentium! Quaerat,
            doloremque, officia nostrum praesentium, ullam quidem ea explicabo
            fugit perspiciatis veritatis voluptatem? Animi pariatur doloremque
            omnis earum eveniet dolore quibusdam laborum accusantium. Iusto a
            deleniti, error veniam aliquam ratione dignissimos cum quas nulla
            itaque, perferendis repellat vel provident nesciunt odit laborum
            repellendus perspiciatis reiciendis. Reiciendis consequuntur eaque,
            pariatur repudiandae illum aspernatur quo doloremque officiis?
            Explicabo deserunt quisquam similique distinctio dolor cumque illo
            praesentium, ipsam corporis, quam exercitationem odit minus
            asperiores sed nisi. Dignissimos maxime repellat temporibus mollitia
            et? Porro perferendis dolorem vel consequuntur quia quam aliquid
            magni voluptates? Provident minima reprehenderit beatae consequuntur
            numquam iste ab velit.
         </p>
      </div>
   );
};

export default Calendar;
