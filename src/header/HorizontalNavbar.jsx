// export default HorizontalNavbar;
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";

function HorizontalNavbar({ toggleSidebar, makeDark, isDark, makeLight }) {
   return (
      <Navbar
         expand="lg"
         className={`horizontal-navbar-color navbar-horizontal ${
            isDark ? "dark" : ""
         }`}
      >
         <Container className="d-flex">
            <nav className="pt-3 fs-0 align-self-center " href="#home">
               <i
                  onClick={toggleSidebar}
                  className="bx bxs-chevrons-right arrow-icon"
               ></i>
            </nav>
            <div className="ms-auto mode align-self-center">
               {isDark ? (
                  <i
                     onClick={makeLight}
                     class="bx bx-sun"
                     style={{ cursor: "pointer" }}
                  ></i>
               ) : (
                  <i
                     onClick={makeDark}
                     className="bx bx-moon"
                     style={{ cursor: "pointer" }}
                  ></i>
               )}
            </div>
         </Container>
      </Navbar>
   );
}

export default HorizontalNavbar;
