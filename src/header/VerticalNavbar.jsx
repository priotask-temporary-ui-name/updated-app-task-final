import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

function VerticalNavbar({ toggleSidebar, sidebarHidden, isDark }) {
   return (
      <Navbar
         className={`sidebar-main ${isDark ? "dark" : ""} d-block ${
            sidebarHidden ? "hidden" : ""
         }`}
      >
         <Navbar.Brand as={Link} to="/">
            <div className="navbar-brand ">
               <span className="">
                  <img
                     className="prio-task-logo"
                     src="https://res.cloudinary.com/dv3x7qoak/image/upload/v1691419534/quill-pen_nkrska.png"
                     alt=""
                  />
               </span>
               <span className={`prio ${isDark ? "dark" : ""}`}>PrioTask</span>
               <span
                  className="ms-5 d-inline d-md-none"
                  onClick={toggleSidebar}
               >
                  <i className="bx bx-menu"></i>
               </span>
            </div>
         </Navbar.Brand>
         <Nav className="d-block menus">
            <Nav.Link
               as={Link}
               to="/"
               className={`d-flex nav-links ${isDark ? "dark" : ""}`}
            >
               <span>
                  <i
                     className={`bx menu-icons ${
                        isDark ? "dark" : ""
                     } bxs-analyse align-self-center animated-navbar`}
                  ></i>
               </span>
               <span
                  className={`align-self-center menu-items ${
                     isDark ? "dark" : ""
                  }`}
               >
                  Start Here
               </span>
            </Nav.Link>
            <Nav.Link
               as={Link}
               to="/stackboard"
               className={`d-flex nav-links ${isDark ? "dark" : ""}`}
            >
               <span>
                  <i
                     className={`bx menu-icons ${
                        isDark ? "dark" : ""
                     } bx-chalkboard align-self-center`}
                  ></i>
               </span>
               <span
                  className={`align-self-center menu-items ${
                     isDark ? "dark" : ""
                  }`}
               >
                  Stack board
               </span>
            </Nav.Link>
            <Nav.Link
               as={Link}
               to="/notifications"
               className={`d-flex nav-links ${isDark ? "dark" : ""}`}
            >
               <span>
                  <i
                     className={`bx menu-icons ${
                        isDark ? "dark" : ""
                     } bx-notification align-self-center`}
                  ></i>
               </span>
               <span
                  className={`align-self-center menu-items ${
                     isDark ? "dark" : ""
                  }`}
               >
                  Notifications
               </span>
            </Nav.Link>
            <Nav.Link
               as={Link}
               to="/calendar"
               className={`d-flex nav-links ${isDark ? "dark" : ""}`}
            >
               <span>
                  <i
                     className={`bx menu-icons ${
                        isDark ? "dark" : ""
                     } bx-calendar-event align-self-center`}
                  ></i>
               </span>
               <span
                  className={`align-self-center menu-items ${
                     isDark ? "dark" : ""
                  }`}
               >
                  Calendar
               </span>
            </Nav.Link>
            <Nav.Link
               as={Link}
               to="/dashboard"
               className={`d-flex nav-links ${isDark ? "dark" : ""}`}
            >
               <span>
                  <i
                     className={`bx menu-icons ${
                        isDark ? "dark" : ""
                     } bxs-dashboard align-self-center`}
                  ></i>
               </span>
               <span
                  className={`align-self-center menu-items ${
                     isDark ? "dark" : ""
                  }`}
               >
                  Dashboard
               </span>
            </Nav.Link>
            <hr className={`${isDark ? "dark" : ""}`} />
         </Nav>
      </Navbar>
   );
}

export default VerticalNavbar;
